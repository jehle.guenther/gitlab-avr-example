# AVR Example

This AVR example demonstrates the usage of GitLab. In the example, the following library is used to control a LC display via the HTL Rankweil Megacard: https://github.com/semiversus/megacard_display

# Developers
* Günther Jena
* Benjamin Reiner
* Daniel Amann
* Raphael Lins
* Angelina Rupp
* Jenny Lampert
* Mihai Marinescu
* Lukas Gsodam
* Marian Biedermann

